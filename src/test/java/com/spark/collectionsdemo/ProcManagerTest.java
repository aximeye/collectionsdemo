package com.spark.collectionsdemo;

import java.util.Set;
import java.util.HashSet;
import org.junit.Test;
import static org.junit.Assert.*;

public class ProcManagerTest {

    @Test
    public void testAddProc() {
	ProcManager pm = new ProcManager();
	pm.setProcSet(new HashSet<Integer>());

	try {
	    assertEquals(true, pm.addProc(1));
	    assertEquals(true, pm.addProc(2));
	    assertEquals(true, pm.addProc(3));
	    assertEquals(true, pm.addProc(4));

	    assertEquals(false, pm.addProc(1));
	    assertEquals(false, pm.addProc(2));

	} catch (Exception e) {
	    e.printStackTrace();
	}
    }

    @Test
    public void testRemoveProc() {
	ProcManager pm = new ProcManager();
	pm.setProcSet(new HashSet<Integer>());

	try {
	    assertEquals(true, pm.addProc(1));
	    assertEquals(true, pm.addProc(2));
	    assertEquals(true, pm.addProc(3));
	    assertEquals(true, pm.addProc(4));

	    assertEquals(true, pm.removeProc(1));
	    assertEquals(true, pm.removeProc(2));
	    assertEquals(false, pm.removeProc(1));
	    assertEquals(false, pm.removeProc(2));

	} catch (Exception e) {
	    e.printStackTrace();
	}
    }

    @Test
    public void testRemoveProc_s() {
	ProcManager pm = new ProcManager();
	pm.setProcSet(new HashSet<Integer>());

	try {
	    assertEquals(true, pm.addProc(1));
	    assertEquals(true, pm.addProc(2));
	    assertEquals(true, pm.addProc(3));
	    assertEquals(true, pm.addProc(4));
	} catch (Exception e) {
	    e.printStackTrace();
	}

	pm.removeProc_s();
	assertEquals(0, pm.getProc_sSize());	
    }

    @Test
    public void testGetProc_sSize() {
	ProcManager pm = new ProcManager();
	pm.setProcSet(new HashSet<Integer>());

	try {
	    assertEquals(true, pm.addProc(1));
	    assertEquals(true, pm.addProc(2));
	    assertEquals(true, pm.addProc(3));
	    assertEquals(true, pm.addProc(4));
	} catch (Exception e) {
	    e.printStackTrace();
	}

	assertEquals(4, pm.getProc_sSize());	
    }
}
