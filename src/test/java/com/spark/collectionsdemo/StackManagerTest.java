package com.spark.collectionsdemo;

import java.util.List;
import java.util.LinkedList;
import org.junit.Test;
import static org.junit.Assert.*;

public class StackManagerTest {

    @Test
    public void testPushReturnAddress() {
	StackManager sm = new StackManager();
	sm.setCallStack(new LinkedList<Integer>());
	
	assertEquals(true, sm.pushReturnAddress(1));
	assertEquals(true, sm.pushReturnAddress(2));
	assertEquals(true, sm.pushReturnAddress(3));
	assertEquals(true, sm.pushReturnAddress(4));
    }
	
    @Test
    public void testPopReturnAddress() {
	StackManager sm = new StackManager();
	sm.setCallStack(new LinkedList<Integer>());
	
	sm.pushReturnAddress(1);
	sm.pushReturnAddress(2);
	sm.pushReturnAddress(3);
	sm.pushReturnAddress(4);

	assertEquals(1, sm.popReturnAddress(), 0);
	assertEquals(2, sm.popReturnAddress(), 0);
	assertEquals(3, sm.popReturnAddress(), 0);
	assertEquals(4, sm.popReturnAddress(), 0);
    }

    @Test
    public void testRemoveReturnAddress_s() {
	StackManager sm = new StackManager();
	sm.setCallStack(new LinkedList<Integer>());
	
	sm.pushReturnAddress(1);
	sm.pushReturnAddress(2);
	sm.pushReturnAddress(3);
	sm.pushReturnAddress(4);

	sm.removeReturnAddress_s();
	assertEquals(0, sm.getStackSize());
    }
	
    @Test
    public void testGetStackSize() {
	StackManager sm = new StackManager();
	sm.setCallStack(new LinkedList<Integer>());
	
	sm.pushReturnAddress(1);
	sm.pushReturnAddress(2);
	sm.pushReturnAddress(3);
	sm.pushReturnAddress(4);

	assertEquals(4, sm.getStackSize());
    }
}
