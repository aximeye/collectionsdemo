package com.spark.collectionsdemo;

import java.util.List;
import java.util.LinkedList;

public class StackManager {
    private List<Integer> callStack;
	
    public StackManager() {}
	
    public List getCallStack() {
	return callStack;
    }

    public void setCallStack(List<Integer> callStack) {
	this.callStack = callStack;
    }
	
    public boolean pushReturnAddress(Integer subroutine) {
	try {
	    return callStack.add(subroutine);
	} catch (Exception e) {
	    e.printStackTrace();
	}
	return false;
    }

    public Integer popReturnAddress() {
	if ( (callStack.iterator()).hasNext() ) {
	    return ((LinkedList<Integer>)callStack).remove();
	}
	return null;
    }

    public void removeReturnAddress_s() {
	callStack.clear();
    }
	
    public int getStackSize() {
	return callStack.size();
    }
}
