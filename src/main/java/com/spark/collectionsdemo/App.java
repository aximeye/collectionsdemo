package com.spark.collectionsdemo;

import java.util.Scanner;
import java.util.logging.Logger;
import java.util.logging.Level;
import java.util.LinkedList;
import java.util.HashSet;

/**
 * Program to demonstrate Collection(Set and List)
 * 
 * @author  Ashish Bhagat
 * @version 1.0
 */
public class App 
{
    private static final Logger logger = Logger.getLogger( App.class.getName() );
	
    public void displayMenu() {
	logger.info("1. Test Set");
	logger.info("2. Test List");
	logger.info("3. Exit");
    }

    public int getUserInput() {
	Scanner reader = new Scanner(System.in);
	int userInput = 0;
		
	while (!reader.hasNextInt()) {
	    reader.next();
	}
	userInput = reader.nextInt();
		
	return userInput;
    }

    public void displayListOperations() {
	System.out.println("1. Add return address to stack");
	System.out.println("2. Remove return address from stack");
	System.out.println("3. Clear stack");
	System.out.println("4. Get stack size");
	System.out.println("5. Exit");
    }

    public void demoList() {
	StackManager sm = new StackManager();
	sm.setCallStack(new LinkedList<Integer>());
	int returnAddress = 1;
	int userInput = 0;

	do {
	    displayListOperations();
	    userInput = getUserInput();

	    switch(userInput) {
	    case 1:
		sm.pushReturnAddress(returnAddress);
		returnAddress++;
		break;
	    case 2:
		sm.popReturnAddress();
		returnAddress--;
		break;
	    case 3:
		sm.removeReturnAddress_s();
		break;
	    case 4:
		System.out.println("Call stack size: " + sm.getStackSize());
		break;
	    case 5:
		return;
	    default:
		System.out.println("Invalid option");
	    }
	} while (5 != userInput);
    }

    public void displaySetOperations() {
	System.out.println("1. Add process");
	System.out.println("2. Remove process");
	System.out.println("3. Clear processes");
	System.out.println("4. Get number of processes");
	System.out.println("5. Exit");
    }

    public void demoSet() {
	ProcManager pm = new ProcManager();
	pm.setProcSet(new HashSet<Integer>());
	int procId = 1;
	int userInput = 0;

	do {
	    displaySetOperations();
	    userInput = getUserInput();

	    switch(userInput) {
	    case 1:
		pm.addProc(procId);
		procId++;
		break;
	    case 2:
		pm.removeProc(procId);
		procId--;
		break;
	    case 3:
		pm.removeProc_s();
		break;
	    case 4:
		System.out.println("Proc_s size: " + pm.getProc_sSize());
		break;
	    case 5:
		return;
	    default:
		System.out.println("Invalid option");
	    }
	} while (5 != userInput);
    }

    public static void main( String[] args ) {
	App app = new App();
		
	do {
	    app.displayMenu();
	    int userInput = app.getUserInput();
			
	    switch (userInput) {
	    case 1:
		app.demoSet();
		break;
	    case 2:
		app.demoList();
		break;
	    case 3:
		System.exit(0);
	    }
	} while (true);
    }
}
