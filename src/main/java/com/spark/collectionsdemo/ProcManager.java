package com.spark.collectionsdemo;

import java.util.Set;

public class ProcManager {
    private Set<Integer> procSet;
	
    public Set getProcSet() {
	return procSet;
    }
	
    public void setProcSet(Set<Integer> procSet) {
	this.procSet = procSet;
    }
	
    public boolean addProc(Integer procId) {
	try {
	    return procSet.add(procId);
	} catch (Exception e) {
	    e.printStackTrace();
	}
	return false;
    }
	
    public boolean removeProc(Integer procId) {
	return procSet.remove(procId);
    }
	
    public void removeProc_s() {
	procSet.clear();
    }
	
    public int getProc_sSize() {
	return procSet.size();
    }
}
